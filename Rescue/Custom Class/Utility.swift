//
//  Utility.swift
//  Rescue
//
//  Created by Megha on 3/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class Utility: NSObject {

    func getDeviceType() ->  String {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return "iPhone"
        }else if UIDevice.current.userInterfaceIdiom == .pad {
            return "iPad"
        }else {
            return "iPod"
        }
    }
    
    //Get Battery Level of device
    func getBatteryLevel() -> Float {
        return UIDevice.current.batteryLevel
    }
    
    //Create Document Directory with App Name
    func createDocumentDirectoryWithAppName() {
        do {
            try FileManager.default.createDirectory(atPath: getDocumentDirectoryPath().path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
    }
    
    func createFileNameWithUserName(str: String)  {
        
        let dataPath = getDocumentDirectoryPath()
        let dataPath1 = dataPath.appendingPathComponent(str)
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath1.path, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print("Error creating directory: \(error.localizedDescription)")
        }
        
        let placesData = NSKeyedArchiver.archivedData(withRootObject: dataPath1)
        UserDefaults.standard.set(placesData, forKey: "USERFOLDERURL")
        UserDefaults.standard.synchronize()
    }
    
    // Calculation of age via given birthday Date.
    func calcAge(birthday:String) -> String{
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd/MM/yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)
        let now: NSDate! = NSDate()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now as Date, options: [])
        let age = calcAge.year
        return String(describing: age!)
    }
    
    // Get document directory path
    func getDocumentDirectoryPath() -> URL {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let dataPath = documentsDirectory.appendingPathComponent(kAppName)
        return dataPath
    }
    
    // convert UIImage to base64 image string
    func convertingStringToBase64(image : UIImage) -> String{
        let imageData : Data = UIImagePNGRepresentation(image)! as Data
        let strBase64 = imageData.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
        return strBase64
    }
    
    // For checking the Null object
    func checkNullObject(_ key : String, dict : NSArray) -> AnyObject {
        var value : AnyObject = "" as AnyObject
        
        if dict.object(at: 0) is NSDictionary {
             value = (dict.value(forKey: key) as! NSArray).object(at: 0) as AnyObject
        }

        return value
    }
    
    //Convert Unix time to IST time 
    func convUnixToIST(interval:TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: interval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation:"IST")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "hh:mm:ssa"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
    
}
