//
//  APIKeys.swift
//  Rescue
//
//  Created by Megha on 3/7/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import Foundation

let kAPI_SERVERBASEURL = "http://consagous.com/rescueApp"

let kContentType = "Content-Type"
let kAccept = "Accept"
let kContentJson = "application/json; charset=utf-8"
let kPostType = "POST"
let kGetType = "Get"

let kAPI_POSTLOGIN = "/Api/login"
let kAPI_POSTSIGNUP = "/Api/registration"
let kAPI_START_ACTIVITY = "/Api/activityStart"
let kAPI_ADD_ACTIVITY = "/Api/activityAdd"
let kAPI_ADD_RESCUE = "/Api/rescueAdd"
let kAPI_RESCUE_LIST = "/Api/rescueList"
let kAPI_RESCUE_ACTIVITY_LIST = "/Api/rescueActivityList"
let kAPI_STOP_ACTIVITY = "/Api/activityStop"
let kAPI_CONTACT_DETAILS = "/Api/userContactList"
let kAPI_DELETE_CONTACT = "/Api/userContactDelete"
let kAPI_ADD_CONTACT = "/Api/userContactAdd"

