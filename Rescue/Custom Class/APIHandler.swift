//
//  APIHandler.swift
//  Rescue
//
//  Created by Megha on 3/7/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import Foundation
import ReachabilitySwift
import SwiftyJSON

class APIHandler {
    
    var reachability = Reachability()!
    var objUtility = Utility()
    
    var delegate :AnyObject?
    var callbackSelector : Selector?
    

    func initWithDelegate(_ aDelegate : AnyObject,aCallbackSel : Selector) -> AnyObject {
        
        self.delegate = aDelegate;
        self.callbackSelector = aCallbackSel;
        
        return self
    }
    
    func callServerAPIForGet(url:NSString) {
        
    }
    
    func callServerAPIForGetWithParameter(url:NSString , dictData: NSDictionary) {
        
    }
    
    func callServerAPIForPost(url:NSString) {
        
    }
    
    func callServerAPIForPostWithParameter(url:NSString , dictRequest: NSDictionary) {
        
        if(reachability.isReachable) {
            var strTemp = String()
            
            let dictReq: [String: Any] = dictRequest.mutableCopy() as! [String : Any]

            do {
                let data = try JSONSerialization.data(withJSONObject:dictReq, options:[])
                let dataString = String(data: data, encoding: String.Encoding.utf8)!
                strTemp = dataString
            } catch {
                print("JSON serialization failed:  \(error)")
            }
            
            var request = URLRequest(url: URL(string: kAPI_SERVERBASEURL+"\(url)")!)
            request.setValue(kContentJson, forHTTPHeaderField: kContentType)  // the request is JSON
            request.setValue(kContentJson, forHTTPHeaderField: kAccept)        // the expected response is also JSON
            
            request.setValue("123", forHTTPHeaderField:kDeviceToken)
            request.setValue(objUtility.getDeviceType(), forHTTPHeaderField:kDeviceType)
            request.httpMethod = kPostType
            
            request.httpBody = strTemp.data(using: .utf8)
            
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print(error ?? "") // some fundamental network error
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {  // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                var dict = NSDictionary()
                if let dataFromString = responseString?.data(using: .utf8, allowLossyConversion: false) {
                    let json = JSON(data: dataFromString)
                    dict = json.object as! NSDictionary
                    DispatchQueue.main.async {
                    _ = self.delegate?.perform(self.callbackSelector!, with:dict)
                    }
                }
            }
            task.resume()
        } else {
            let alert = UIAlertController(title:NSLocalizedString(kAlertTitle_NO_NETWORK, comment: kAlertTitle_NO_NETWORK) , message:NSLocalizedString(kAlertMessage_NO_NETWORK, comment: kAlertMessage_NO_NETWORK) , preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK), style: .default, handler: nil))
            return
        }
    }
    
    
    func callWeatherAPI(url:NSString) {
        
        if(reachability.isReachable) {
            
            let urlString = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let directionsURL = NSURL(string: urlString)
            
            var request = URLRequest(url:directionsURL as! URL)
            
            request.setValue(kContentJson, forHTTPHeaderField: kContentType)  // the request is JSON
            request.setValue(kContentJson, forHTTPHeaderField: kAccept)        // the expected response is also JSON
            
            request.httpMethod = kPostType
            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                guard let data = data, error == nil else {
                    print(error ?? "") // some fundamental network error
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {  // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                }
                
                let responseString = String(data: data, encoding: .utf8)
                var dict = NSDictionary()
                if let dataFromString = responseString?.data(using: .utf8, allowLossyConversion: false) {
                    let json = JSON(data: dataFromString)
                    dict = json.object as! NSDictionary
                    DispatchQueue.main.async {
                        _ = self.delegate?.perform(self.callbackSelector!, with:dict)
                    }
                }
            }
            task.resume()
        } else {
            let alert = UIAlertController(title:NSLocalizedString(kAlertTitle_NO_NETWORK, comment: kAlertTitle_NO_NETWORK) , message:NSLocalizedString(kAlertMessage_NO_NETWORK, comment: kAlertMessage_NO_NETWORK) , preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK), style: .default, handler: nil))
            return
        }
    }
    
    
    
}

