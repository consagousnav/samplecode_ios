//
//  Constant.swift
//  Rescue
//
//  Created by Megha on 3/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import Foundation

let kAppName = "Rescue"

//API Header constant
let kDeviceType = "uidType"
let kDeviceToken = "uidNumber"

//LoginVC
let kForgotPassword  = "Forgot Password?"
let kEnterEmail = "Please input your email:"
let kConfirm = "Confirm"
let kCancel = "Cancel"
let kSavedUserEmail = "userEmail"
let kEmail = "email"
let kPassword = "password"


//SignUpVC
let kChooseOption = "Choose Option"
let kBrowsePhoto = "Browse Photo"
let kTakePhoto = "Take Photo"


//Alert Constant
let kAlertTitle_NO_NETWORK = "No Network Connection"
let kAlertMessage_NO_NETWORK = "Internet connection not available."
let kServerIssue = "Have some server error."
let kOK = "OK"

//Login Response
let kData = "data"
let kUserAddress = "address"
let kUserAge = "age"
let kCity = "city"
let kCountry = "country"
let kDOB = "dateOfBirth"
let kUserEmail = "email"
let kLatitude = "latitude"
let kLongitude = "longitude"
let kUserContact = "mobileNo"
let kUserName = "name"
let kUserSurname = "surName"
let kGender = "sex"
let kUserToken = "token"
let kUserId = "userId"
let kUserImg = "user_img"
let kContactId = "contactId"

//SignUp Response


//Start Activity
let kActivityID = "activityId"







