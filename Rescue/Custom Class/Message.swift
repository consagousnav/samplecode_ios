

import Foundation

let kAlertTitle_Alert       = "Alert"
let kAlertTitle_Message     = "Message"
let kAlertTitle_Confirm     = "Confirm"
let kAlertTitle_Add         = "Add"
let kAlertTitle_DialysisService = "Dialysis Service."

let kAlertMessage_ServerNotWorking      = "Server is not responding, it may be due to slow internet connectivity.\nPlease try after some time."
let kAlertMessage_InternetNotAvailable  = "No internet connectivity."

let kAlertMessage_EmptyAllCheck         = "\nPlease enter value for all fields."


let kAlertMessage_EmptyCategory         = "\nPlease enter value for Category."
let kAlertMessage_EmptyType        = "\nPlease enter value for Types."


let kAlertMessage_EmptyFirstname        = "\nPlease enter first name."
let kAlertMessage_ValidFirstname        = "\nPlease enter valid first name."

let kAlertMessage_ValidMiddlename       = "\nPlease enter valid middle name."

let kAlertMessage_EmptyLastname         = "\nPlease enter last name."
let kAlertMessage_ValidLastname         = "\nPlease enter valid last name."

let kAlertMessage_EmptyEmailId          = "\nPlease enter email id."
let kAlertMessage_ValidEmailId          = "\nPlease enter valid email."

let kAlertMessage_EmptyPassword         = "\nPlease enter password."
let kAlertMessage_ValidPassword         = "\nPlease enter valid password."
let kAlertMessage_AlphaNumericPassword  = "\nPassword should be alpha numeric."

let kAlertMessage_EmptyConfirmPassword  = "\nPlease enter confirm password."
let kAlertMessage_ValidConfirmPassword  = "\nPlease enter valid confirm password."

let kAlertMessage_PasswordLength        = "\nPassword should have atleast 8 characters."
let kAlertMessage_PasswordNotmatch      = "\nPassword do not match."

let kAlertMessage_BlankCurrentPwd       = "\nPlease enter current password."
let kAlertMessage_BlankNewPwd           = "\nPlease enter new password."

let kAlertMessage_EmptyGender           = "\nPlease select gender."
let kAlertMessage_ValidGender           = "\nPlease select valid gender."

let kAlertMessage_EmptyAddress          = "\nPlease enter address."
let kAlertMessage_ValidAddress          = "\nPlease enter valid address."

let kAlertMessage_EmptyCity             = "\nPlease enter city."
let kAlertMessage_ValidCity             = "\nPlease enter valid city."


let kAlertMessage_EmptyState            = "\nPlease select state."
let kAlertMessage_ValidState            = "\nPlease select valid state."

let kAlertMessage_EmptyLandmark         = "\nPlease enter landmark."
let kAlertMessage_ValidLandmark         = "\nPlease enter valid landmark."

let kAlertMessage_EmptyPhoneNumber      = "\nPlease enter phone number."
let kAlertMessage_EmptyCountaryCode      = "\nPlease enter Country code"
let kAlertMessage_ValidPhoneNumber      = "\nPlease enter valid phone number."
let kAlertMessage_ValidLandLineNumber   = "\nPlease enter valid Landline number."

let kAlertMessage_EmptyPincode          = "\nPlease enter pincode."
let kAlertMessage_ValidPincode          = "\nPlease enter valid zipcode."

let kAlertMessage_TermsAndCondition     = "\nPlease accept Terms & Conditions."

let kAlertMessage_RegistrationSuccessful    = " !! Congratulation your account has been created and an activation link has been sent to the email address registered by you.If you do not get the email in your inbox , kindly check your junk or spam folder. Note that you must activate the account by clicking on the activation link.In case you don't receive the activation link within next 15 minutes email us at care@khattamettha.com."

let kAlertMessage_EmptyEmailIdOrUsername    = "\nPlease enter email id / username."
let kAlertMessage_ValidEmailIdOrUsername    = "\nPlease enter valid email / username."

let kAlertMessage_EmptySelectPinCode    = "\nPlease select Area/Pin code."
let kAlertMessage_EmptySelectArea       = "\nPlease select city."

let kAlertMessage_EmptyName             = "\nPlease enter name."
let kAlertMessage_ValidName             = "\nPlease enter valid name."

let kAlertMessage_EmptyDOB              = "\nPlease select date of birth."
let kAlertMessage_EmptyRelation         = "\nPlease select relationship."

let kAlertMessage_DeletePatientConfirm  = "\nAre sure want to delete Patient."
let kAlertMessage_DeleteDoctorConfirm   = "\nAre sure want to delete Doctor."
let kAlertMessage_DeleteAddressConfirm  = "\nAre sure want to delete Address."
let kAlertMessage_SelectAddressType     = "\nPlease select address Type."

let kAlertMessage_MedicineQuantity      = "Enter Quantity"
let kAlertMessage_MedicineNotFound      = "Search Medicine did not matched from our list, If you want to add this medicine into the order list  please write the medicine name properly and we will call you back."
let kAlertMessage_MedicineExist         = "This medicine already exist into order list. Do you want to add it again into the order list?"

let kAlertMessage_EnterOtp      = "Enter OTP"

let kAlertMessage_EmptyDate     = "\nPlease select date."
let kAlertMessage_EmptyRemark   = "\nPlease enter remark."
let kAlertMessage_EmptyOrderSequence    = "\nPlease enter sequence of medicines (as they appear in prescription)."
let kAlertMessage_DialysisServiceMessage = "Dialysis is a process for removing waste and excess water from the blood, and is used primarily as an artificial replacement for lost kidney function in people with renal failure. Dialysis may be used for those with an acute disturbance in kidney function. Dialysis is regarded as a \"holding measure\" until a renal transplant can be performed, or sometimes as the only supportive measure in those for whom a transplant would be inappropriate. \n Supplymedicine.com helps you to generate the dialysis enquiry at the city of your choice. Please Select the city from list below as per your requirement ."

let kAlertMessage_SelectDoctor          = "\nPlease select doctor."
let kAlertMessage_SelectPatient         = "\nPlease select patient."
let kAlertMessage_SelectAddress         = "\nPlease add address."

let kAlertMessage_UploadPrescription    = "\nPlease upload prescription."
let kAlertMessage_EmptyNearestLandmark  = "\nPlease enter nearest landmark."

let kAlertMessage_UploadPrescriptionLater   = "Please handover xerox copy of prescription to the delivery boy."
let kAlertMessage_MinimumOrderAmount    = "Your order total amount is less than the minimum amount required for this vendor. So, please select your choice mentioned above."


let kAlertMessage_UpdateProfileSuccess   = "Your profile has been successfully updated."

let kAlertMessage_EmptyFromDate     = "\nPlease select from date."
let kAlertMessage_EmptyToDate     = "\nPlease select to date."

let kAlertMessage_ChangePassword   = "Your password has been successfully updated."
let kAlertMessage_EmptyReview   = "\nPlease enter review."
let kAlertMessage_EmptyRating   = "\nPlease enter star rating."
let kAlertMessage_AddReviewSuccess   = "Review added successfully."
let kAlertMessage_BookEquipmentSuccess   = "Your Booking have been successfully submited.\nWe'll be contact as soon as possible."
let kAlertMessage_OrderConfirmation     = " you have successfully placed your order and you shall receive the Purchase Order details via Email and SMS shortly.Your order No is "
let kAlertMessage_BookDialysis   = "Your message has been submitted."

let kAlertMessage_MandatoryFields =  "Manadtory fields are empty"
let kAlertMessage_ResponseFailed =  "Unable to continue :: Please check the provided information again."
