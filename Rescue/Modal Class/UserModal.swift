//
//  UserModal.swift
//  Rescue
//
//  Created by Megha Neema on 11/03/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class UserModal: NSObject {
    
    var strForAddress: String!
    var IntForAge: Int!
    var strForCity: String!
    var strForCountry: String!
    var strForName: String!
    var dateForBirth: String!
    var strForEmail: String!
    var strForLatitude: String!
    var strForLongitude: String!
    var IntForID: Int!
    var strForSurname: String!
    var strForToken: String!
    var strForProfilePic: String!
    var strForContact: String!
    var strForGender : String!
    
    //Create Singleton object
    static let sharedInstance : UserModal = {
        let instance = UserModal()
        return instance
    }()


    // This function for convert JSON object to Data Modal This is
    func objectFormat(_ dataDict : NSDictionary?) -> UserModal {
        let objData = UserModal.sharedInstance
        
        if dataDict != nil {
            
            objData.strForAddress = Utility().checkNullObject(kUserAddress, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.IntForAge = (Utility().checkNullObject(kUserAge, dict:(dataDict!.value(forKey: kData) as! NSArray)) as! NSString).integerValue
            objData.strForCity = Utility().checkNullObject(kCity, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForCountry = Utility().checkNullObject(kCountry, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForName = Utility().checkNullObject(kUserName, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForSurname = Utility().checkNullObject(kUserSurname, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForEmail = Utility().checkNullObject(kUserEmail, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.dateForBirth = Utility().checkNullObject(kDOB, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForLatitude = Utility().checkNullObject(kLatitude, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForLongitude = Utility().checkNullObject(kLongitude, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.IntForID = (Utility().checkNullObject(kUserId, dict:(dataDict!.value(forKey: kData) as! NSArray)) as! NSString).integerValue
            objData.strForToken = Utility().checkNullObject(kUserToken, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForProfilePic = Utility().checkNullObject(kUserImg, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForContact = Utility().checkNullObject(kUserContact, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String
            objData.strForGender = Utility().checkNullObject(kGender, dict: (dataDict!.value(forKey: kData) as! NSArray)) as! String

        }
        return objData
    }
    
    // This function for convert JSON object to Data Modal This is Main Function
    func arrayDictToObj(_ arrDictData : NSArray? ) -> NSMutableArray{
        
        let arrDataObj = NSMutableArray()
        if arrDictData != nil{
            for dicData in arrDictData! {
                let rootObj = objectFormat(dicData as? NSDictionary)
                arrDataObj.add(rootObj)
            }
            return arrDataObj
        }
        return arrDataObj
    }

}


