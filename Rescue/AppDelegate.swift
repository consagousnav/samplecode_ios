//
//  AppDelegate.swift
//  Rescue
//
//  Created by Megha on 3/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit
import PKHUD
import Firebase
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var objUtility = Utility()

    
    func appDelegate () -> AppDelegate
    {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Crashlytics().debugMode = true
        Fabric.with([Crashlytics.self])
        objUtility.createDocumentDirectoryWithAppName()
        // this code for create white navigation title
        UINavigationBar.appearance().barTintColor = UIColor(red: 223.0/255.0, green: 93.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        FIRApp.configure()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
   }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: Hudview Methods
    func showHudView() {
        // Copy below line to call this method in any view
        // AppDelegate().appDelegate().showHudView()
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
    }
    
    func hideHudView() {
        // Copy below line to call this method in any view
        // AppDelegate().appDelegate().hideHudView()
        PKHUD.sharedHUD.hide()
    }
    
    func hideHudViewSuccess() {
        // Copy below line to call this method in any view
        // AppDelegate().appDelegate().hideHudViewSuccess()
        //        PKHUD.sharedHUD.contentView = PKHUDSuccessView()
        PKHUD.sharedHUD.hide()
    }
    
    func hideHudViewFailure() {
        // Copy below line to call this method in any view
        // AppDelegate().appDelegate().hideHudViewFailure()
        //        PKHUD.sharedHUD.contentView = PKHUDErrorView()
        PKHUD.sharedHUD.hide()
    }
    
    func showHudTextView(_ textMessage: String) {
        PKHUD.sharedHUD.contentView = PKHUDTextView(text: textMessage)
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide()
    }
    
    
    // This method for logout user
    func logout() {
        UserDefaults.standard.set("NO", forKey: "ISLOGIN")
        UserDefaults.standard.set(nil, forKey: "LOGINUSERVALUE")
        UserDefaults.standard.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "InitialVC") as! InitialVC
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
       
        self.window?.rootViewController = nvc
        self.window?.makeKeyAndVisible()
    }
    
    
}

