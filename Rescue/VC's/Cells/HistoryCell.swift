//
//  HistoryCell.swift
//  Rescue
//
//  Created by Consagous on 16/03/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    @IBOutlet weak var lblForSource: UILabel!
    @IBOutlet weak var lblForDesti: UILabel!
    @IBOutlet weak var lblForRideID: UILabel!
    @IBOutlet weak var lblForStartTime: UILabel!
    @IBOutlet weak var lblForEndTime: UILabel!
    @IBOutlet weak var btnForInfor: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
