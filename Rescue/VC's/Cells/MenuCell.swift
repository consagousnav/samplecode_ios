//
//  MenuCell.swift
//  Rescue
//
//  Created by Consagous on 18/03/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {

    @IBOutlet weak var lblForName = UILabel()
    @IBOutlet weak var imgForName = UIImageView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
