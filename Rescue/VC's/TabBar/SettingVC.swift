//
//  SettingVC.swift
//  Rescue
//
//  Created by Aegis Infotech on 07/03/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class SettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

  
    
    @IBOutlet weak var  tblForSettings: UITableView!
    @IBOutlet weak var lblForError: UILabel!
    
    var arrForSettings = NSMutableArray()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Setting"
        // Do any additional setup after loading the view.
        
        let dict1: NSMutableDictionary = ["Name": "Profile", "image": UIImage(named: "profile")! as UIImage]
        let dict2: NSMutableDictionary = ["Name": "Contact", "image": UIImage(named: "cont")! as UIImage]
        let dict3: NSMutableDictionary = ["Name": "About Us", "image": UIImage(named: "profile")! as UIImage ]
        let dict4: NSMutableDictionary = ["Name": "FAQ", "image": UIImage(named: "profile")! as UIImage ]
        let dict5: NSMutableDictionary = ["Name": "Log Out", "image": UIImage(named: "logout")! as UIImage ]
        
        
        arrForSettings = [dict1,dict2,dict3,dict4,dict5]
        tblForSettings.reloadData()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
    // MARK: - Table View Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForSettings.count
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.selectionStyle = .none
        
        cell.lblForName?.text = ((arrForSettings.object(at: indexPath.row) as AnyObject).value(forKey: "Name") as! String)
        
        cell.imgForName?.image = ((arrForSettings.object(at: indexPath.row) as AnyObject).value(forKey: "image") as! UIImage)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell!.contentView.backgroundColor = UIColor(red: 219.0/255.0, green: 66.0/255.0, blue: 64.0/255.0, alpha: 1.0)
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath as IndexPath)
        cell!.contentView.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       switch indexPath.row {
        case 0:
            
            let signUpObj = storyboard!.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            signUpObj.isEdit = true
            self.navigationController?.pushViewController(signUpObj, animated: true)
            break
            
        case 1:
            
            let contactObj = storyboard!.instantiateViewController(withIdentifier: "ContactVC") as! ContactVC
            self.navigationController?.pushViewController(contactObj, animated: true)
            break

        case 2:
            
//            let findProObj = storyboard!.instantiateViewController(withIdentifier: "SendFeedbackVC") as! SendFeedbackVC
//            self.navigationController?.pushViewController(findProObj, animated: true)
            break
        
            case 3:
        break
        
       case 4:
        let alert = UIAlertController(title: "Message", message:"Are you sure you want to Log Out?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                
                
                 AppDelegate().appDelegate().logout()
                
                print("default")
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
        self.present(alert, animated: true, completion: nil)
        
       default:
        break

        
        }
    }

}
