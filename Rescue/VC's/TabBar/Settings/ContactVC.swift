//
//  ContactVC.swift
//  Rescue
//
//  Created by Consagous on 18/03/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit
import EVContactsPicker

class ContactVC: UIViewController,EVContactsPickerDelegate { //contactObj
    
    let contactPicker = EVContactsPickerViewController()
    @IBOutlet weak var  tblForContact: UITableView!
    var arrForContact : NSMutableArray = []
    var arrForEmergency : NSMutableArray = []
    var arrForSharing : NSMutableArray = []
    
    var typeOfContact : String?
    
    public func didChooseContacts(_ contacts: [EVContactProtocol]?) {
        if let cons = contacts {
            for con in cons {
                print("\(con.fullname())")
                let dictTemp = NSMutableDictionary()
                
                dictTemp.setValue(con.fullname(), forKey: "name")
                dictTemp.setValue(con.phone, forKey: "mobileNo")
                dictTemp.setValue(con.phone, forKey: "secondaryMobileNo")
                arrForContact.add(dictTemp)
            }
        }
        addContactDetails()
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblForContact.tableFooterView = UIView()
        self.tblForContact.delegate = self
        self.tblForContact.dataSource = self
        
        if arrForContact.count > 0 {
            arrForContact.removeAllObjects()
        }
        if arrForSharing.count > 0 {
            arrForSharing.removeAllObjects()
        }
        if arrForEmergency.count > 0 {
            arrForEmergency.removeAllObjects()
        }
        
        fetchContactDetails()
        AppDelegate().appDelegate().showHudView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        super.viewWillDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onPlusButtonTap(_ sender: Any) {
        
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        
        settingsActionSheet.addAction(UIAlertAction(title:"Add Emergency Contact", style:UIAlertActionStyle.default, handler:{ action in
            self.contactPicker.delegate = self
            self.typeOfContact = "Emergency"
            self.navigationController?.pushViewController(self.contactPicker, animated: true)
        }))
        
        settingsActionSheet.addAction(UIAlertAction(title:"Add Sharing Contact", style:UIAlertActionStyle.default, handler:{ action in
            self.contactPicker.delegate = self
            self.typeOfContact = "Sharing"
            self.navigationController?.pushViewController(self.contactPicker, animated: true)
        }))
        
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.cancel, handler:nil))
        settingsActionSheet.view.tintColor =  UIColor(red: 219.0/255.0, green: 66.0/255.0, blue: 64.0/255.0, alpha: 1.0)
        present(settingsActionSheet, animated:true, completion:nil)
    }
    
    func fetchContactDetails() {
        
        let apiHandler = APIHandler()
        let dictUser: NSMutableDictionary = [kUserId:UserModal.sharedInstance.IntForID]
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(ContactVC.getResponseContacts(_:)))
        apiHandler.callServerAPIForPostWithParameter(url: kAPI_CONTACT_DETAILS as NSString, dictRequest: dictUser)
    }
    
    func getResponseContacts(_ objResponse:NSMutableDictionary) {
        print(objResponse)
        AppDelegate().appDelegate().hideHudView()

        if objResponse.value(forKey:"status") as! String == "1" {
            
            arrForContact = (objResponse.value(forKey: "data")! as! NSArray).mutableCopy() as! NSMutableArray
            
            for i in 0..<arrForContact.count {
                var strName = String()
                strName = (((arrForContact.object(at: i)) as AnyObject).value(forKey:"contactType") as? String)!
                
                switch strName {
                case "Emergency" :
                    arrForEmergency.add(((arrForContact.object(at: i)) as AnyObject))
                case "Sharing" :
                    arrForSharing.add(((arrForContact.object(at: i)) as AnyObject))
                default:
                    strName = ""
                }
            }
            
            DispatchQueue.main.async {
                self.tblForContact.reloadData()
            }
        } else {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: "No Contact to display.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Mark :- API to add Contact Details
    func addContactDetails() {
        AppDelegate().appDelegate().showHudView()
        let apiHandler = APIHandler()
        let dictUser: NSMutableDictionary = [kUserId:UserModal.sharedInstance.IntForID,"contactType":typeOfContact!,"contact":arrForContact]
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(ContactVC.getResponseAddContacts(_:)))
        apiHandler.callServerAPIForPostWithParameter(url: kAPI_ADD_CONTACT as NSString, dictRequest: dictUser)
    }
    
    func getResponseAddContacts(_ objResponse:NSMutableDictionary) {
        print(objResponse)
        AppDelegate().appDelegate().hideHudView()
        
        if objResponse.value(forKey:"status") as! String == "1" {
            AppDelegate().appDelegate().showHudView()
            if arrForContact.count > 0 {
                arrForContact.removeAllObjects()
            }
            if arrForSharing.count > 0 {
                arrForSharing.removeAllObjects()
            }
            if arrForEmergency.count > 0 {
                arrForEmergency.removeAllObjects()
            }
            fetchContactDetails()
        } else {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: "No Contact to display.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    //Mark :- API to delete contact
    func actionForDeleteContact(contactID:Int) {
        AppDelegate().appDelegate().showHudView()
        let apiHandler = APIHandler()
        let dictUser: NSMutableDictionary = [kContactId:contactID]
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(ContactVC.getResponseDeleteContact(_:)))
        apiHandler.callServerAPIForPostWithParameter(url: kAPI_DELETE_CONTACT as NSString, dictRequest: dictUser)
    }
    
    
    func getResponseDeleteContact(_ objResponse:NSMutableDictionary) {
        print("objResponse",objResponse)
        AppDelegate().appDelegate().hideHudView()

        if objResponse.value(forKey:"status") as! String == "1" {
            if arrForContact.count > 0 {
                arrForContact.removeAllObjects()
            }
            if arrForSharing.count > 0 {
                arrForSharing.removeAllObjects()
            }
            if arrForEmergency.count > 0 {
                arrForEmergency.removeAllObjects()
            }
            
            fetchContactDetails()
        } else {
            let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Alert) , message: NSLocalizedString( kAlertMessage_ResponseFailed, comment:  kAlertMessage_ResponseFailed), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK) , style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension ContactVC : UITableViewDelegate,UITableViewDataSource {
    
    // MARK: - Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return arrForSharing.count
        case 1:
            return arrForEmergency.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var strName = String()
        
        switch section {
        case 0 :
            strName = "Sharing"
        case 1 :
            strName = "Emergency"
        default:
            strName = ""
        }

        return strName
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        // Background color
        view.tintColor = UIColor(red: 219.0/255.0, green: 66.0/255.0, blue: 64.0/255.0, alpha: 1.0)

        // Text Color
        let header = (view as! UITableViewHeaderFooterView)
        header.textLabel?.textColor = UIColor.white
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count = Int()
        
        if arrForEmergency.count > 0 {
            count = 1
        }
        if arrForSharing.count > 0 {
            count = count+1
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        
        switch indexPath.section {
        case 0 :
            cell.lblForSource.text = (arrForSharing.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
            cell.lblForDesti.text = (arrForSharing.object(at: indexPath.row) as AnyObject).value(forKey: "mobileNo") as? String
        case 1 :
            cell.lblForSource.text = (arrForEmergency.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
            cell.lblForDesti.text = (arrForEmergency.object(at: indexPath.row) as AnyObject).value(forKey: "mobileNo") as? String
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            switch indexPath.section {
            case 0 :
                self.actionForDeleteContact(contactID:(((arrForSharing.object(at: indexPath.row) as AnyObject).value(forKey: "id")) as! NSString).integerValue)
                break
            case 1:
                self.actionForDeleteContact(contactID:(((arrForEmergency.object(at: indexPath.row) as AnyObject).value(forKey: "id")) as! NSString).integerValue)
                 break
            default:
                print("No Row Selected")
                break
            }
        }
    }
}



