//
//  MapVC.swift
//  Rescue
//
//  Created by Aegis Infotech on 07/03/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import KCFloatingActionButton
import MessageUI

enum JSONError: String, Error {
    case NoData = "ERROR: no data"
    case ConversionFailed = "ERROR: conversion from JSON failed"
}

class MapVC: UIViewController,KCFloatingActionButtonDelegate,MFMessageComposeViewControllerDelegate {
    
    let locationManager = CLLocationManager()
    @IBOutlet weak var map: MKMapView!

    @IBOutlet weak var btnForStopActivity: UIButton!
    @IBOutlet weak var btnForStartActivity: UIButton!
    
    
    //create floating button
    var fab = KCFloatingActionButton()
    
    var annotTitle = Int()
    var oldLocation : CLLocation? = nil
    
    var objUtility = Utility()
    var activityID : NSInteger = 0
    var flag = true
    var temperature : Double = 0.0
    var sunsetTime : String?
    var locationArray: NSMutableArray = []
    
    var timerToActivityAdd: Timer? = nil
    
    var loggedUser : FIRUser!
    var authReference : FIRAuth!
    var userID = "default"

    @IBOutlet weak var btnToGOCurrent: UIButton!
    @IBOutlet weak var btnForWeather: UIButton!
    @IBOutlet weak var segmentForMapType: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.title = "Home"
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.allowsBackgroundLocationUpdates =  true
        }
        
        annotTitle = 0
        map.delegate = self
        map.mapType = .standard
        map.isZoomEnabled = true
        map.isScrollEnabled = true
        map.showsUserLocation = true
        
        if let coor = map.userLocation.location?.coordinate{
            map.setCenter(coor, animated: true)
        }
         authReference = FIRAuth.auth()
         layoutFAB()
         fab.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        map.showsUserLocation = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        map.showsUserLocation = true
    }
    
    @IBAction func onSegmentControlValueChanged(_ sender: UISegmentedControl) {
        let newIndex = sender.selectedSegmentIndex
        switch newIndex {
        case 0:
            map.mapType = .standard
            segmentForMapType.tintColor = UIColor(red: 219.0/255.0, green: 66.0/255.0, blue: 64.0/255.0, alpha: 1.0)
        case 1:
            map.mapType = .satellite
            segmentForMapType.tintColor = UIColor.white
        case 2:
            map.mapType = .hybrid
            segmentForMapType.tintColor = UIColor.white
        default:
            map.mapType = .standard
            segmentForMapType.tintColor = UIColor(red: 219.0/255.0, green: 66.0/255.0, blue: 64.0/255.0, alpha: 1.0)
        }
    }

    //Mark :- API calling to start tracking
    @IBAction func actionForStartActivity(_ sender: Any) {
        locationManager.startUpdatingLocation()
        btnForStartActivity.isHidden = true
        //Start Tracking
            let apiHandler = APIHandler()
            let dictUser: NSMutableDictionary = [kUserId:UserModal.sharedInstance.IntForID,kLatitude:String(describing: oldLocation?.coordinate.latitude),kLongitude:String(describing: oldLocation?.coordinate.longitude)]
            _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(MapVC.getResponseStartActivity(_:)))
            apiHandler.callServerAPIForPostWithParameter(url: kAPI_START_ACTIVITY as NSString, dictRequest: dictUser)
    }
    
    
    func getResponseStartActivity(_ objResponse:NSMutableDictionary) {
        print("objResponse",objResponse)
        
        if objResponse.value(forKey:"status") as! String == "1" {

            btnForStopActivity.isHidden = false
            //select option to Rescue
            fab.isHidden = false
            
            let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Message) , message: objResponse.value(forKey:"message") as? String, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK) , style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            activityID = objResponse.value(forKey: kActivityID) as! NSInteger
            
            timerToActivityAdd = Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(callAddActivity), userInfo: nil, repeats: true)

        } else {
            let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Alert) , message: NSLocalizedString( kAlertMessage_ResponseFailed, comment:  kAlertMessage_ResponseFailed), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK) , style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func callForRescue() {

        let apiHandler = APIHandler()
        let dictUser: NSMutableDictionary = [kActivityID:activityID ,kLatitude:String(describing: oldLocation?.coordinate.latitude),kLongitude:String(describing: oldLocation?.coordinate.longitude)]
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(MapVC.getResponseRescue(_:)))
        apiHandler.callServerAPIForPostWithParameter(url: kAPI_ADD_RESCUE as NSString, dictRequest: dictUser)
    }
    
    func getResponseRescue(_ objResponse:NSMutableDictionary) {
        print("objResponse",objResponse)
        if objResponse.value(forKey:"status") as! String == "1"  {
            print(objResponse.value(forKey:"message")!)
            timerToActivityAdd?.invalidate()
            timerToActivityAdd = nil
        } else {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: kServerIssue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func callAddActivity() {
        if locationArray.count > 0 {
              fetchWeatherInfoFromLocation(location:oldLocation!)
        }else {
            print("locationArray is Empty")
        }
    }
    
    //Mark :- API calling to add tracking
    @IBAction func addActivity(arrOfLocation : NSMutableArray) {
        
        let apiHandler = APIHandler()
        
        let dictForRequest =  NSMutableDictionary()
        dictForRequest.setObject(UserModal.sharedInstance.IntForID, forKey: kUserId as NSCopying)
        dictForRequest.setObject(activityID, forKey: kActivityID as NSCopying )
        dictForRequest.setObject(arrOfLocation, forKey: "latlong" as NSCopying)
        dictForRequest.setObject("", forKey: "rescueNote" as NSCopying)
        dictForRequest.setObject(objUtility.getBatteryLevel(), forKey: "phoneBattery" as NSCopying)
        dictForRequest.setObject(temperature, forKey: "temperature" as NSCopying)
        dictForRequest.setObject(sunsetTime!, forKey: "sunsetTime" as NSCopying)
        
        print(dictForRequest)
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(MapVC.getResponseAddActivity(_:)))
        apiHandler.callServerAPIForPostWithParameter(url: kAPI_ADD_ACTIVITY as NSString, dictRequest: dictForRequest)
        locationArray.removeAllObjects()
    }
    
    func getResponseAddActivity(_ objResponse:NSMutableDictionary) {
        print("objResponse",objResponse)
        if objResponse.value(forKey:"status") as! String == "1"  {
            print("Activity added successfully")
        } else {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: kServerIssue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Mark :- API calling to stop tracking
    @IBAction func actionForStopActivity(_ sender: Any) {
        btnForStopActivity.isHidden = true
        btnForStartActivity.isHidden = false
        fab.isHidden = true

        let apiHandler = APIHandler()
        let dictUser: NSMutableDictionary = [kUserId:UserModal.sharedInstance.IntForID,kActivityID:activityID ,kLatitude:String(describing: oldLocation?.coordinate.latitude),kLongitude:String(describing: oldLocation?.coordinate.longitude)]
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(MapVC.getResponseToStop(_:)))
        apiHandler.callServerAPIForPostWithParameter(url: kAPI_STOP_ACTIVITY as NSString, dictRequest: dictUser)
    }
    
    func getResponseToStop(_ objResponse:NSMutableDictionary) {
        print(objResponse)
        timerToActivityAdd?.invalidate()
        timerToActivityAdd = nil
    }
    
    //Mark :- API calling to get weather details
    func fetchWeatherInfoFromLocation(location:CLLocation) {
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            print(location)
            
            if error != nil {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if (placemarks?.count)! > 0 {
                if(self.btnForWeather.isSelected) {
                     self.getWeatherDetails(fromLocation:(placemarks?[0].subLocality!)!)
                     return
                }
                
                let pm = placemarks?[0]
                self.getWeatherDetails(fromLocation:(pm?.locality!)!)
            }
            else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    func getWeatherDetails(fromLocation:String) {
        
        let apiHandler = APIHandler()
        
        let urlString : String = "http://api.openweathermap.org/data/2.5/weather?q=\(fromLocation)&APPID=653cb32e84fee91f5cfde858f99770e0&units=imperial"
        
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(MapVC.getWeatherResponse(_:)))
        apiHandler.callWeatherAPI(url: urlString as NSString)
    }
    
    func getWeatherResponse(_ objResponse:NSMutableDictionary) {
        print("objResponse",objResponse)
        if objResponse["main"] != nil && objResponse["sys"] != nil {
            
            temperature = (objResponse["main"] as! NSDictionary).value(forKey: "temp") as! Double
            let sTime: TimeInterval = ((objResponse["sys"] as! NSDictionary).value(forKey: "sunset"))! as! TimeInterval
            sunsetTime = objUtility.convUnixToIST(interval:sTime)
            
            if btnForWeather.isSelected {
                getWeatherInfo(sender:btnForWeather)
                btnForWeather.isSelected = false
                return
            }
            self.addActivity(arrOfLocation:locationArray)
        }else {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: kServerIssue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func getWeatherInfo(_ sender: Any) {
        if(oldLocation != nil) {
            if temperature != 0.0 || sunsetTime != nil {
                let alert = UIAlertController(title: "Weather Details", message: "Temperature:\(temperature)°F\nSunset Time:\(sunsetTime!)", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                fetchWeatherInfoFromLocation(location:self.oldLocation!)
                btnForWeather.isSelected = true
            }
        }
    }
   
    @IBAction func actionToGoCurrentLocation(_ sender: Any) {
        if oldLocation != nil {
            setZoomLevel(center: CLLocationCoordinate2D(latitude: (oldLocation?.coordinate.latitude)!, longitude: (oldLocation?.coordinate.longitude)!))
        }else {
            btnToGOCurrent.isSelected = true
            locationManager.startUpdatingLocation()
        }
    }

    func layoutFAB() {
        //Start a call
        fab.addItem("Call", icon: UIImage(named: "Float_Help")) { item in
            if let url = URL(string: "tel://8103419548") {
                UIApplication.shared.openURL(url)
            }
        }
        //Send SMS to a person
        fab.addItem("SMS", icon: UIImage(named: "Float_Help")){ item in
            let messageVC = MFMessageComposeViewController()

            if MFMessageComposeViewController.canSendText() {
                messageVC.body = "test Message"
                messageVC.recipients = ["8103419548"]
                messageVC.messageComposeDelegate = self
                self.present(messageVC, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: kAlertTitle_Alert, message: "Message Cannot be send", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        //Start Chatting
        fab.addItem("Chat", icon: UIImage(named: "Float_Help")){ item in
            
            self.authReference?.signInAnonymously() { (user, error) in
                if error != nil {
                    print(error!)
                    return
                }
                self.loggedUser = user
                
                let chatObj = self.storyboard!.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
                chatObj.navigationItem.title = "Chat"
                chatObj.senderDisplayName = UserModal.sharedInstance.strForName
                chatObj.senderId = self.loggedUser.uid
                self.navigationController?.pushViewController(chatObj, animated: true)
            }
        }
        
        //Start Video Calling
        fab.addItem("Video", icon: UIImage(named: "Float_Help")){ item in
            let videoObj = self.storyboard!.instantiateViewController(withIdentifier: "VideoCallVC") as! VideoCallVC
            videoObj.navigationItem.title = "Video Call"
            self.navigationController?.pushViewController(videoObj, animated: true)
        }

        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func KCFABOpened(_ fab: KCFloatingActionButton) {
        self.callForRescue()
        let alert = UIAlertController(title: kAlertTitle_Alert, message: "Notification sent to contacts,you will be rescued soon..", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func KCFABClosed(_ fab: KCFloatingActionButton) {
        fab.isHidden = true
        
        self.btnForStartActivity.isHidden = false
        self.btnForStopActivity.isHidden = true
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        var strMessage = String()
        switch result.rawValue {
        case 0:
            strMessage = "Message Sending Cancelled."
            break
        case 1:
           strMessage = "Message Sent Successfully"
            break
        case 2:
           strMessage = "Message Sending Failed"
            break
        default:
           strMessage = ""
            break
        }
        let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Message) , message: NSLocalizedString(strMessage, comment: strMessage), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK) , style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        controller.dismiss(animated: true)
    }
}

extension MapVC : MKMapViewDelegate {
    //Mark:- MapView Delegate Methods
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        print("Annotation selected")
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        let identifier = "pin"
        var view : MKPinAnnotationView
        if let dequeueView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView{
            dequeueView.annotation = annotation
            view = dequeueView
        }else{
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            let centre = mapView.centerCoordinate as CLLocationCoordinate2D
            oldLocation =  CLLocation(latitude: centre.latitude, longitude: centre.longitude)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        view.pinTintColor = .red
        return view
    }
}

extension MapVC:CLLocationManagerDelegate {
    
    //Mark:- Location manager delegate mthods
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let age:TimeInterval = (location?.timestamp.timeIntervalSinceNow)!
        if age > 120 {
            return
        }
        if Int((location?.horizontalAccuracy)!) < 0 {
            return
        }
        
        if btnToGOCurrent.isSelected {
            btnToGOCurrent.isSelected = false
            return
        }

        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        let distance : CLLocationDistance = (location?.distance(from: oldLocation!))!
        if (distance >= 10 && Int((location?.horizontalAccuracy)!) <= 20) {
            self.oldLocation = location!
            
            let annot:MKPointAnnotation = MKPointAnnotation()
            annot.coordinate = locValue
            annotTitle += 1
            annot.title = "\(annotTitle)"
            self.map.addAnnotation(annot)
            setZoomLevel(center: locValue)
            let dictTemp : NSMutableDictionary = [:]
            dictTemp.setValue((self.oldLocation?.coordinate.latitude)!, forKey: kLatitude)
            dictTemp.setValue((self.oldLocation?.coordinate.longitude)!, forKey: kLongitude)
            locationArray.add(dictTemp)
        }
        
        if(flag) {
            setZoomLevel(center: locValue)
            flag = false
        }
    }
    
    func setZoomLevel(center : CLLocationCoordinate2D) {
        
        let spanX =  0.0005
        let spanY =  0.0005
        
        let newRegion = MKCoordinateRegion(center:center , span: MKCoordinateSpanMake(spanX, spanY))
        map.setRegion(newRegion, animated: true)
    }
}

extension MFMailComposeViewController {
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.isTranslucent = false
        navigationBar.isOpaque = false
        navigationBar.barTintColor = UIColor.red
        navigationBar.tintColor = UIColor.black
    }
}

