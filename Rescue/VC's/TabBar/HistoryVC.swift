//
//  HistoryVC.swift
//  Rescue
//
//  Created by Megha Neema on 12/03/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var  tblForHistory: UITableView!
    @IBOutlet weak var lblForError: UILabel!
    
    var arrForHistory = NSMutableArray()
    
    
    
    var activityID : NSInteger = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "History"
        
        callRescueHistoryAPI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    //Call Rescue History
    func callRescueHistoryAPI() {
        let apiHandler = APIHandler()
        
        let dictForRequest =  NSMutableDictionary()
        dictForRequest.setObject(UserModal.sharedInstance.IntForID, forKey: kUserId as NSCopying)
        _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(HistoryVC.getResponsRescueHistory(_:)))
        apiHandler.callServerAPIForPostWithParameter(url: kAPI_RESCUE_LIST as NSString, dictRequest: dictForRequest)
    }
    
    
    func getResponsRescueHistory(_ objResponse:NSMutableDictionary) {
        if objResponse.value(forKey:"status") as! String == "1"  {
            for i in 0..<(objResponse.value(forKey:"data") as AnyObject).count {
                arrForHistory.add((objResponse.value(forKey:"data") as AnyObject).object(at: i))
            }
            tblForHistory.reloadData()
            } else {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: kServerIssue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Table View Methods
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrForHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell", for: indexPath) as! HistoryCell
        cell.btnForInfor.tag = indexPath.row
        cell.lblForRideID.text = (arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "activityId") as? String
        cell.lblForDesti.text = (arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "destination") as? String
        cell.lblForSource.text = (arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "source") as? String
        cell.lblForEndTime.text = (arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "endTime") as? String
        cell.lblForStartTime.text = (arrForHistory .object(at: indexPath.row) as AnyObject).value(forKey: "startTime") as? String
         cell.btnForInfor.addTarget(self, action: #selector(HistoryVC.actionONifnoButton(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func actionONifnoButton(_ sender:UIButton){
       let link = (arrForHistory .object(at: sender.tag) as AnyObject).value(forKey: "url") as? String
        
        let webObj = storyboard!.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        webObj.strForLink = link!
        self.navigationController?.pushViewController(webObj, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}



