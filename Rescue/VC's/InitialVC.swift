//
//  InitialVC.swift
//  Rescue
//
//  Created by Megha on 3/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit

class InitialVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var textForUsername: UITextField!
    @IBOutlet weak var textForPassword: UITextField!
    
    var objUtility = Utility()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // This check for first Time login
        if UserDefaults.standard.object(forKey: "ISLOGIN") as? String == "YES"{
            _ = UserModal.sharedInstance.objectFormat(UserDefaults.standard.value(forKey: "LOGINUSERVALUE") as! NSDictionary?)
            let tabObj = storyboard!.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
            self.navigationController?.pushViewController(tabObj, animated: true)
        }
        
        //set delegates for textfield
        textForUsername.delegate = self
        textForPassword.delegate = self
        
        //Need to remove
        textForUsername.text = "pramod.jain@consagous.com"
        textForPassword.text = "123"
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark :- IBAction Methods
    @IBAction func tapOnLoginButton(_ sender: Any) {
        
        // To resgin key board when we tap Login button
        self.view.endEditing(true)
        
        if (textForUsername.text?.isEmpty)! || (textForPassword.text?.isEmpty)! {
            
            let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Alert) , message: NSLocalizedString( kAlertMessage_MandatoryFields, comment:  kAlertMessage_MandatoryFields), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK) , style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        } else {
            let apiHandler = APIHandler()
            AppDelegate().appDelegate().showHudView()
            let dictUser: NSMutableDictionary = [kEmail:textForUsername.text ?? "",kPassword:textForPassword.text ?? ""]
            _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(LoginVC.getResponseLogin(_:)))
            apiHandler.callServerAPIForPostWithParameter(url: kAPI_POSTLOGIN as NSString, dictRequest: dictUser)
        }
        
    }
    
    func getResponseLogin(_ objResponse:NSMutableDictionary) {
        print("objResponse",objResponse)
        AppDelegate().appDelegate().hideHudView()
        if objResponse.value(forKey:"status") as! String == "1" {
            objUtility.createFileNameWithUserName(str: textForUsername.text!)
            _ = UserModal.sharedInstance.objectFormat(objResponse)
            
            // set this value for one time login
            
            UserDefaults.standard.set("YES", forKey: "ISLOGIN")
            UserDefaults.standard.set(objResponse, forKey: "LOGINUSERVALUE")
            UserDefaults.standard.synchronize()
            
            let tabObj = storyboard!.instantiateViewController(withIdentifier: "MainTabVC") as! MainTabVC
            self.navigationController?.pushViewController(tabObj, animated: true)
        } else {
            let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Alert) , message: NSLocalizedString( kAlertMessage_ResponseFailed, comment:  kAlertMessage_ResponseFailed), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK) , style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //Mark :- Action for forget password
    @IBAction func tapOnForgotPassword(_ sender: Any) {
        
        let alertController = UIAlertController(title: NSLocalizedString(kForgotPassword, comment: kForgotPassword), message: NSLocalizedString(kEnterEmail, comment: kEnterEmail), preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: NSLocalizedString(kConfirm, comment: kConfirm), style: .default) { (_) in
            if let field : UITextField = alertController.textFields?[0] {
                UserDefaults.standard.set(field.text!, forKey: kSavedUserEmail)
                UserDefaults.standard.synchronize()
            } else {
                let alert = UIAlertController(title: NSLocalizedString(kAlertTitle_Alert, comment: kAlertTitle_Alert) , message: NSLocalizedString( kAlertMessage_MandatoryFields, comment:  kAlertMessage_MandatoryFields), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString(kOK, comment: kOK) , style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
        let cancelAction = UIAlertAction(title: NSLocalizedString(kCancel, comment: kCancel), style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = NSLocalizedString(kEmail, comment: kEmail)
        }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}



