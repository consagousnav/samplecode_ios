//
//  SignUpVC.swift
//  Rescue
//
//  Created by Megha on 3/6/17.
//  Copyright © 2017 consagous. All rights reserved.
//

import UIKit
import MRCountryPicker
import AMTagListView
import EVContactsPicker

class SignUpVC: UIViewController,MRCountryPickerDelegate,customPickerDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate,EVContactsPickerDelegate {
    
    
     var typeOfContact : String?
    
    let contactPicker = EVContactsPickerViewController()
     var arrForContact : NSMutableArray = []
    
    public func didChooseContacts(_ contacts: [EVContactProtocol]?) {
        if let cons = contacts {
            for con in cons {
                print("\(con.fullname())")
                let dictTemp = NSMutableDictionary()
                
                if typeOfContact == "Sharing"{
                    tagListSharing.addTag(con.fullname())
 
                }
                else if typeOfContact == "Emergency"{
                    tagListEmergancy.addTag(con.fullname())
                }
                dictTemp.setValue(con.fullname(), forKey: "name")
                dictTemp.setValue(con.phone, forKey: "mobileNo")
                dictTemp.setValue(con.phone, forKey: "secondaryMobileNo")
                arrForContact.add(dictTemp)
            }
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBOutlet weak var tagListEmergancy     : AMTagListView!
    @IBOutlet weak var tagListSharing       : AMTagListView!
    @IBOutlet weak var txtForLastName       : UITextField!
    @IBOutlet weak var txtFroFirstName      : UITextField!
    @IBOutlet weak var txtForPassword       : UITextField!
    @IBOutlet weak var txtFroEmailID        : UITextField!
    @IBOutlet weak var btnForProfilePic     : UIButton!
    @IBOutlet weak var txtForMobile         : UITextField!
    @IBOutlet weak var txtForDOB            : UITextField!
    @IBOutlet weak var txtForAge            : UITextField!
    @IBOutlet weak var txtForAddress1       : UITextField!
    @IBOutlet weak var txtForSex            : UITextField!
    @IBOutlet weak var txtForCity           : UITextField!
    @IBOutlet weak var txtForCountry        : UITextField!
    @IBOutlet weak var btnForUserImage      : UIButton!
    
    var customPicker: CustomPicker!
    var arrForGender: NSMutableArray = []
    
    var isEdit = Bool()
    
    @IBOutlet weak var countryPicker: MRCountryPicker!
    @IBOutlet weak var countryPhoneCodeView: UIView!
    
    // create picker view object
    let picker = UIImagePickerController()
    
    var isYearPicked = false
    
    var datePicker = GMDatePicker()
    var dateFormatter = DateFormatter()
    
    // create utility object to access global method
    var objUtility = Utility()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        AMTagView.appearance().textFont = UIFont(name: "Futura", size: 11)
        AMTagView.appearance().tagColor = UIColor(red: 223.0/255.0, green: 93.0/255.0, blue: 91.0/255.0, alpha: 1.0)
        
        btnForUserImage.layer.cornerRadius = btnForUserImage.frame.height/2
        btnForUserImage.clipsToBounds = true
        
        countryPicker.countryPickerDelegate = self
        countryPicker.showPhoneNumbers = false
        countryPicker.setCountry("IN")
        
        arrForGender = ["MALE","FEMALE"]
        picker.delegate = self
        
        // set date formate
        dateFormatter.dateFormat = "dd/MM/yyyy"  //"dd MM yyyy"
        
        if isEdit ==  true{
            txtForLastName.text = UserModal.sharedInstance.strForSurname
            txtFroFirstName.text = UserModal.sharedInstance.strForName
//            txtForPassword.text = UserModal.sharedInstance.strForPa
            txtFroEmailID.text = UserModal.sharedInstance.strForEmail
//            btnForProfilePic.text
            txtForMobile.text = UserModal.sharedInstance.strForContact
            txtForDOB.text = UserModal.sharedInstance.dateForBirth
//            txtForAge.text = UserModal.sharedInstance.IntForAge
            txtForAddress1.text = UserModal.sharedInstance.strForAddress
            txtForSex.text = UserModal.sharedInstance.strForGender
            txtForCity.text = UserModal.sharedInstance.strForCity
            txtForCountry.text = UserModal.sharedInstance.strForCountry
//            btnForUserImage
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    func countryPhoneCodePicker(_ picker: MRCountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        txtForCountry.text = name
        
    }
    
    
    @IBAction func onTapOnRegistrationDoneButton(sender: AnyObject){
        isValidData()
    }
    
    @IBAction func onTapONContactButtonIcon(_ sender: UIButton) {
        
        if sender.tag == 0 {
            self.typeOfContact = "Emergency"
         }
        else if sender.tag == 1 {
            self.typeOfContact = "Sharing"
        }
        
     self.contactPicker.delegate = self
     self.navigationController?.pushViewController(self.contactPicker, animated: true)
     
    }
 
    
    func isValidData() {
        
        var strAlertMessage = ""
        var isValid:Bool = true
        
        
        let strFirstName        = txtFroFirstName.text!.trimmedString()
        let strLastName         = txtForLastName.text!.trimmedString()
        let strPassword         = txtForPassword.text!.trimmedString()
        let strMobile           = txtForMobile.text!.trimmedString()
        let strEmail            = txtFroEmailID.text!.trimmedString()
        let strDOB              = txtForDOB.text!.trimmedString()
        let strAge              = txtForAge.text!.trimmedString()
        let strAddress          = txtForAddress1.text!.trimmedString()
        let strGender           = txtForSex.text!.trimmedString()
        let strCountry          = txtForCountry.text!.trimmedString()
        let strCity             = txtForCity.text!.trimmedString()
        
        
        if strFirstName.isEmpty && strLastName.isEmpty && strPassword.isEmpty && strMobile.isEmpty && strEmail.isEmpty && strDOB.isEmpty && strAge.isEmpty && strAddress.isEmpty && strGender.isEmpty{
            strAlertMessage += kAlertMessage_EmptyAllCheck
            isValid = false
        } else {
            // Validation check for Firstname
            if strEmail.isEmpty {
                // Check for Blank Username
                strAlertMessage += kAlertMessage_EmptyEmailIdOrUsername
                isValid = false
            }
            if strEmail.isEmpty {
                // Check for Blank Username
                strAlertMessage += kAlertMessage_EmptyEmailIdOrUsername
                isValid = false
            }else if !(strEmail.isValidEmail()) {
                // Check for Valid EmailId
                strAlertMessage += kAlertMessage_ValidEmailIdOrUsername
                isValid = false
            }
            
            if strPassword.isEmpty {
                // Check for Blank Username
                strAlertMessage += kAlertMessage_EmptyPassword
                isValid = false
            }else if !strPassword.isValidPassword() {
                // Check for Valid Password
                strAlertMessage += kAlertMessage_ValidPassword
                isValid = false
            }
            
            // Validation check for Landmark
            if strMobile.isEmpty {
                // Check for Blank Landmark
                strAlertMessage += kAlertMessage_EmptyPhoneNumber
                isValid = false
            } else if !strMobile.isValidPhoneNumber() {
                // Check for Valid Landmark
                strAlertMessage += kAlertMessage_ValidPhoneNumber
                isValid = false
            }
            
        }
        if (isValid == true) {
            
            let dictRequest =  NSMutableDictionary()
            
            dictRequest.setObject(strFirstName, forKey: "name" as NSCopying)
            dictRequest.setObject(strLastName, forKey: "surName" as NSCopying)
            dictRequest.setObject(strPassword, forKey: "password" as NSCopying)
            dictRequest.setObject(strMobile, forKey: "mobileNo" as NSCopying)
            dictRequest.setObject(strEmail, forKey: "email" as NSCopying)
            dictRequest.setObject(strDOB, forKey: "dateOfBirth" as NSCopying)
            dictRequest.setObject(strAge, forKey: "age" as NSCopying)
            dictRequest.setObject(strAddress, forKey: "address" as NSCopying)
            dictRequest.setObject(strGender, forKey: "sex" as NSCopying)
            dictRequest.setObject(strCountry, forKey: "country" as NSCopying)
            dictRequest.setObject(strCity, forKey: "city" as NSCopying)
            
            let base64Str = objUtility.convertingStringToBase64(image: btnForUserImage.currentImage!)
            dictRequest.setObject(base64Str, forKey: "user_img"  as NSCopying)
            
            let apiHandler = APIHandler()
            _ = apiHandler.initWithDelegate(self, aCallbackSel:#selector(SignUpVC.getResponseSignUP(_:)))
            apiHandler.callServerAPIForPostWithParameter(url: kAPI_POSTSIGNUP as NSString, dictRequest: dictRequest)
            
            return
            
        } else {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: strAlertMessage, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    //Created By : Rajesh Sijoria
    //Date : 20 / 07 /16.
    //Description : Method to get response of login
    //Update by :
    //Reason :
    func getResponseSignUP(_ objResponse:NSMutableDictionary) {
        //  print(aDict)
        
        
        if objResponse.allKeys.count == 0 {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: kServerIssue, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let errCode = objResponse.object(forKey: "status") as! Bool
        
        
        if errCode == true {
            let alert = UIAlertController(title: kAlertTitle_Alert, message: objResponse.object(forKey: "message") as? String, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    _ = self.navigationController?.popViewController(animated: true)
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
           self.present(alert, animated: true, completion: nil)
            
            return
            
        }else
        {
            let alert = UIAlertController(title: "Alert", message:objResponse.object(forKey: "errorMessage") as! String?, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                case .default:
                    print("default")
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK:  IMAGE
    @IBAction func actionONImageViewForChangeImage(sender: AnyObject)
    {
        let settingsActionSheet: UIAlertController = UIAlertController(title:nil, message:nil, preferredStyle:UIAlertControllerStyle.actionSheet)
        settingsActionSheet.addAction(UIAlertAction(title:"Photo Library", style:UIAlertActionStyle.default, handler:{ action in
            self.photoFromLibrary()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Camera", style:UIAlertActionStyle.default, handler:{ action in
            self.shootPhoto()
        }))
        settingsActionSheet.addAction(UIAlertAction(title:"Cancel", style:UIAlertActionStyle.cancel, handler:nil))
        present(settingsActionSheet, animated:true, completion:nil)
    }
    
    
    func photoFromLibrary() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
        // picker.popoverPresentationController?.barButtonItem = sender
    }
    
    func shootPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    func noCamera(){
        let alertVC = UIAlertController(
            title: "No Camera",
            message: "Sorry, this device has no camera",
            preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: "OK",
            style:.default,
            handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    
    //MARK: Image View Delegate Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
        
    {
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        
        chosenImage = resizeImage(image: chosenImage, newWidth: 300)
        btnForUserImage.setImage(chosenImage, for: .normal)
        
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x:0, y:0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}

extension SignUpVC {
    
    
    @IBAction func onTapOnCountryButton(sender: AnyObject){
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations:{
            
            self.countryPhoneCodeView.frame = CGRect(x: 0, y: self.view.frame.size.height - self.countryPhoneCodeView.frame.size.height, width: self.view.frame.size.width , height: self.countryPhoneCodeView.frame.size.height)
        })
        
    }
    
    @IBAction func onTapOnCountryDoneButton(sender: AnyObject) {
        UIView.animate(withDuration: 0.5, animations:{
            
            self.countryPhoneCodeView.frame = CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width , height: self.countryPhoneCodeView.frame.size.height)
        })
    }
    
    
    @IBAction func onTapOnDOB(sender: AnyObject) {
        view.endEditing(true)
        // set up for Date picker
        setupDatePicker()
    }
    
    // MARK: - delegate of Custom Picker
    func doneFromPicker(_ textField: UITextField,indexData:Int) {
        
    }
    
    func insertData(_ textField: UITextField, indexData: Int) -> String {
        if textField == txtForSex  {
            let strData =  arrForGender.object(at: indexData) as! String
            return strData
        }
        
        
        return ""
    }
    
    //InputAccesory
    func addDoneButton(_ textField : UITextField) {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: view, action: #selector(UIView.endEditing(_:)))
        
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
}


extension SignUpVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        customPicker = CustomPicker()
        customPicker.delegateCustomPicker=self
        
        if textField == txtForSex{
            if arrForGender.count>0 {
                customPicker.setdata(arrForGender, tfInput: textField)
                textField.inputView=customPicker
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        customPicker.removeAllViews()
        
    }
}

extension SignUpVC: GMDatePickerDelegate {
    
    func gmDatePicker(_ gmDatePicker: GMDatePicker, didSelect date: Date){
        print(date)
        txtForDOB?.text = dateFormatter.string(from: date)
        
        txtForAge.text = "\(objUtility.calcAge(birthday: (txtForDOB!.text)!)) Years"
    }
    
    func gmDatePickerDidCancelSelection(_ gmDatePicker: GMDatePicker) {
        
    }
    
    fileprivate func setupDatePicker() {
        
        datePicker.delegate = self
        datePicker.config.startDate = Date()
        datePicker.config.animationDuration = 0.5
        datePicker.config.cancelButtonTitle = "Cancel"
        datePicker.config.confirmButtonTitle = "Confirm"
        datePicker.config.contentBackgroundColor = UIColor(red: 253/255.0, green: 253/255.0, blue: 253/255.0, alpha: 1)
        datePicker.config.headerBackgroundColor = UIColor(red: 244/255.0, green: 244/255.0, blue: 244/255.0, alpha: 1)
        datePicker.config.confirmButtonColor = UIColor.black
        datePicker.config.cancelButtonColor = UIColor.black
        datePicker.show(inVC: self)
    }
}


